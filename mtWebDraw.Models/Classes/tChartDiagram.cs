﻿using System;

namespace mtWebDraw.Models
{
    /// <summary>
    /// Web图Diagrams信息表
    /// </summary>
    public class tChartDiagram
    {

        /// <summary>
        /// 标识
        /// </summary>
        public virtual Int64 ID { get; set; }

        /// <summary>
        /// Web图标识
        /// </summary>
        public virtual Guid ChartID { get; set; }

        /// <summary>
        /// 图表名称
        /// </summary>
        public virtual String DiagramName { get; set; }

        /// <summary>
        /// 图表内容
        /// </summary>
        public virtual String DiagramContent { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        public virtual Boolean IsDelete { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual DateTime CreateTime { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public virtual DateTime UpdateTime { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        public virtual Guid UpdateUser { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public virtual String Remark { get; set; }

    }
}