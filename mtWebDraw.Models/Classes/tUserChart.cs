﻿using System;

namespace mtWebDraw.Models
{
    /// <summary>
    /// 用户图表授权表
    /// </summary>
    public class tUserChart
    {
    
        /// <summary>
        /// 标识
        /// </summary>
        public virtual Int64 ID { get; set; }
        
        /// <summary>
        /// 用户标识
        /// </summary>
        public virtual Guid UserID { get; set; }
        
        /// <summary>
        /// 图表标识
        /// </summary>
        public virtual Guid ChartID { get; set; }
        
        /// <summary>
        /// 授权代码
        /// </summary>
        public virtual Byte Code { get; set; }
        
    }
}